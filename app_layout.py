import dash
from dash import html, dcc

def create_layout():
    # Define Input components
    input_components = html.Div(
        [
            dcc.Input(id='nom_commune', type='text', placeholder='Nom commune', style={'color': 'blue','fontSize': '20px'}),
            dcc.Input(id='code_postal', type='text', placeholder='Code postal', style={'color': 'blue','fontSize': '20px'}),
            dcc.Input(id='adresse_nom_voie', type='text', placeholder='Adresse nom voie', style={'color': 'blue', 'fontSize': '20px'}),
            dcc.Input(id='code_type_local', type='text', placeholder='Code type local', style={'color': 'blue', 'fontSize': '20px'}),
        ],
        style={'margin-bottom': '20px'}
    )

    # Define AgGrid component
    ag_grid_component = html.Div(
        [
            html.H3("Evalimmo"),
            html.Div(id='ag-grid-output')
        ],
        style={'margin': '20px 0'}
    )

    # map_iframe
    map_iframe = html.Iframe(id='map-iframe', width='100%', height='600px')

    # Assemblage de tous les composants pour la mise en page finale
    layout = html.Div([input_components, ag_grid_component, map_iframe])

    return layout