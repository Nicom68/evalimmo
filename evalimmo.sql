DROP DATABASE IF EXISTS evalimmo;
CREATE DATABASE evalimmo;
USE evalimmo;

CREATE TABLE `adresse` (
  `id_adresse` INTEGER PRIMARY KEY,
  `adresse_numero` INTEGER,
  `adresse_suffixe` VARCHAR(255),
  `adresse_nom_voie` VARCHAR(255),
  `adresse_code_voie` VARCHAR(255),
  `code_commune` INTEGER,
  `nom_commune` VARCHAR(255),
  `code_postal` INTEGER,
  `code_departement` INTEGER,
  `longitude` FLOAT,
  `latitude` FLOAT
);


CREATE TABLE `bien_immo` (
  `id_parcelle` VARCHAR PRIMARY KEY,
  `code_type_local` ENUM('1', '2', '3'),
  `type_local` ENUM ('Appartement', 'Maison', 'Dépendance'),
  `surface_reelle_bati` INTEGER,
  `nombre_pieces_principales` INTEGER,
  `surface_terrain` INTEGER,
  `id_adresse` INTEGER,
  FOREIGN KEY (`id_adresse`) REFERENCES `adresse` (`id_adresse`)
);



CREATE TABLE `terrain` (
  `id_terrain` INTEGER PRIMARY KEY,
  `code_nature_culture` VARCHAR(255), 
  `code_nature_culture_speciale` VARCHAR(255), 
  `id_parcelle` VARCHAR,
  FOREIGN KEY (`id_parcelle`) REFERENCES `bien_immo` (`id_parcelle`)
);



CREATE TABLE `vente` (
  `id_mutation` VARCHAR PRIMARY KEY,
  `date_mutation` DATE,
  `numero_disposition` INTEGER,
  `nature_mutation` VARCHAR(255),
  `valeur_fonciere` FLOAT,
  `code_commune` INTEGER,
  `id_parcelle` VARCHAR,
  FOREIGN KEY (`id_parcelle`) REFERENCES `bien_immo` (`id_parcelle`)
);

CREATE TABLE `lot` (
  `id_lot` INTEGER PRIMARY KEY,
  `numero_lot` INTEGER,
  `surface_carrez` INTEGER,
  `id_parcelle` VARCHAR,
  FOREIGN KEY (`id_parcelle`) REFERENCES `bien_immo` (`id_parcelle`)
);

CREATE TABLE `ann_culture` (
  `code_nature_culture` VARCHAR(255), 
  `nature_culture` VARCHAR(255)
);

CREATE TABLE `ann_culture_speciale` (
  `code_nature_culture_speciale` VARCHAR(255), 
  `nature_culture_speciale` VARCHAR(255)
);
