import pandas as pd

# lecture du fichier  CSV dans un dataframe
df = pd.read_csv('31.csv')
df.info()


# création d'un fichier échantillon en csv
limited_df = df.head(100)
print(limited_df)
limited_df = df.head(100)


# # récupération des noms de colonnes du dataframe dans une liste
def get_column_names(dataframe):
    return list(dataframe.columns)


column_names = get_column_names(df)
print(column_names)


# Création d'un dataframe avec les colonnes "column_name"
column_names_df = pd.DataFrame({'Column Names': column_names})

# Ecrire le dataframe "column_name" dans un fichier excel
column_names_df.to_excel('column_names.xlsx', index=False)

