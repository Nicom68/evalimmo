-- 1) test id bien_immo
SELECT * FROM bien_immo
WHERE id_immo= 1;


-- 2) Recherche biens immobiliers à l'adresse "5, Avenue Jean Rieux"

SELECT adresse_numero, adresse_nom_voie, numero_lot, sur
FROM bien_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN lot ON bien_immo.id_immo = lot.id_immo
WHERE adresse_numero = 5 AND adresse_nom_voie="AV JEAN RIEUX";

-- 3) comptage des mutations pour une adresse avec numéro (5 AV JEAN RIEUX)
SELECT COUNT (id_mutation)
FROM vente
JOIN vente_immo ON vente.id_vente = vente_immo.id_vente
JOIN bien_immo ON vente_immo.id_immo = bien_immo.id_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
WHERE adresse_nom_voie = 'AV JEAN RIEUX' AND adresse_numero = 5
ORDER BY adresse_numero;


-- 4) Recherche bien immobilier ayant fait l'objet de plusieurs ventes dans l'année

SELECT adresse.*
FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
WHERE bien_immo.id_immo IN (
    SELECT vente_immo.id_immo
    FROM vente_immo
    JOIN vente ON vente_immo.id_vente = vente.id_vente
    GROUP BY vente_immo.id_immo
    HAVING COUNT(vente.id_mutation) > 5
);

-- 5) Recherche  bien_immo + valeur_immo pour une date de mutation donnée 
SELECT * FROM bien_immo
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE date_mutation ="2019-01-05"; 

-- 6) Recherche bien_immo + adresse + valeur_immo > 300 000

SELECT * FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE adresse_nom_voie = "AV JEAN RIEUX" AND valeur_fonciere > 300000;


-- 7) Recherche bien_immo + adresse_commune + valeur_immo > 200 000 
SELECT * FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE nom_commune = "Caraman" AND valeur_fonciere > 200000;

-- 8) test comptage des biens vendus par adresse (bien_immo + adresse + valeur_immo + id_parcelle)

SELECT COUNT(id_mutation) FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente ON bien_immo.id_immo = vente.id_vente
WHERE adresse_nom_voie = "RUE BAYARD";



-- 9) test comptage des mutations (bien_immo + adresse + valeur_immo + id_parcelle)

SELECT COUNT (id_mutation)
FROM vente
JOIN vente_immo ON vente.id_vente = vente_immo.id_vente
JOIN bien_immo ON vente_immo.id_immo = bien_immo.id_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN lot ON bien_immo.id_immo=lot.id_immo
WHERE adresse_nom_voie = 'AV JEAN RIEUX' AND id_parcelle="31555810AC0332"
ORDER BY adresse_numero;

-- 10) test comptage des biens_immo vendus avec terrain > 3 000

SELECT * from bien immo
WHERE surface_terrain > 3000;

-- 11) recherche bien immo par date de vente (date_mutation)
SELECT *
FROM bien_immo
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente_immo ON vente.id_vente = vente_immo.id_vente
WHERE date_mutation = "2019-10-16"

-- 12) recherche du bien immobilier le plus cher en Haute Garonne

SELECT *
FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE vente.valeur_fonciere = (
    SELECT MAX(valeur_fonciere)
    FROM vente
);


-- 13) recherche de la maison la moins chère en Haute Garonne

SELECT *
FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE vente.valeur_fonciere = (
    SELECT MIN(valeur_fonciere)
    FROM vente
) AND code_type_local=1;

-- 14) recherche du bien immobilier avec le plus de terrain en Haute Garonne (surface_terrain)

SELECT *
FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE surface_terrain = (
    SELECT MAX(surface_terrain)
    FROM bien_immo
);


-- 15) recherche de lots (surface_carrez) > 50 m2 

SELECT *
FROM bien_immo
JOIN vente_immo ON bien_immo.id_immo= vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
JOIN lot ON bien_immo.id_immo = lot.id_lot
WHERE surface_carrez >"50";

--  16) recherche de lots (surface_carrez) > 150 m2 

SELECT *
FROM bien_immo
JOIN lot ON bien_immo.id_immo = lot.id_lot
WHERE surface_carrez >"150";


--  17) Trouver le nombre de lots pour 6 rue d'Aubuisson 

SELECT id_mutation, numero_lot, adresse_numero, adresse_nom_voie
FROM lot
JOIN bien_immo ON lot.id_immo = bien_immo.id_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE adresse_nom_voie = "RUE D'AUBUISSON" AND adresse_numero="6";


-- 18) Recherche de bien avec un parc à Toulouse

SELECT DISTINCT id_parcelle,adresse_nom_voie, nom_commune, surface_terrain, nature_culture_speciale
FROM bien_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN terrain ON bien_immo.id_immo = terrain.id_immo
JOIN culture_speciale ON terrain.code_nature_culture_speciale = culture_speciale.code_nature_culture_speciale
WHERE culture_speciale.code_nature_culture_speciale = 'PARC' AND nom_commune = 'Toulouse';
 

-- 19) Recherche de biens immobiliers avec de la vigne, prix de vente et surface de terrain

SELECT adresse_nom_voie, nom_commune, nature_culture,valeur_fonciere,surface_terrain, code_type_local
FROM bien_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN vente_immo ON bien_immo.id_immo= vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
JOIN terrain ON bien_immo.id_immo = terrain.id_immo
JOIN nature_culture ON terrain.code_nature_culture= nature_culture.code_nature_culture
WHERE nature_culture = "vignes";


-- 20) Recherche du nombre de transactions par commune 
-- (classement par ordre décroissant)

SELECT nom_commune, COUNT(id_mutation) AS nombre_ventes
FROM VENTE
JOIN vente_immo ON vente.id_vente = vente_immo.id_vente
JOIN bien_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
GROUP BY nom_commune
ORDER BY nombre_ventes DESC;

-- 21) Recherche par commune de bien_immobilier, affichage de la valeur foncière, du type de bien, et du numéro de rue
SELECT adresse_nom_voie, nom_commune, valeur_fonciere, code_type_local
FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE "nom_commune" = "Balma";

-- 22) Recherche par commune ou nom de rue de bien_immobilier, affichage de la surface habitable en m2, du type de bien
  
SELECT adresse_nom_voie, nom_commune, surface_reelle_bati, code_type_local
FROM bien_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
JOIN lot ON bien_immo.id_immo = lot.id_immo
WHERE adresse_nom_voie = "AV CRAMPEL";











