# main.py

import dash
import sqlite3
from app_layout import create_layout
from dash.dependencies import Input, Output
import plotly.graph_objects as go

# Initialisation de l'application DASH
app = dash.Dash(__name__)

# Récupération de la mise en page à partir du fichier "app_layout.py"
app.layout = create_layout()

@app.callback(
    Output('map-iframe', 'srcDoc'),
    [Input('nom_commune', 'value'),
     Input('code_postal', 'value'),
     Input('adresse_nom_voie', 'value'),
     Input('code_type_local', 'value')]
)

# fonction pour mettre à jour la carte
def update_map(nom_commune, code_postal, adresse_nom_voie, code_type_local):
    
    # connexion à la base de données
    connection = sqlite3.connect("evalimmo.db")
    connection.row_factory = sqlite3.Row  # renvoie des dictionnaires
    mycursor = connection.cursor()

    # requête
    query =    """
    SELECT * 
    FROM adresse 
    JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse 
    JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo 
    JOIN vente ON vente_immo.id_vente = vente.id_vente 
    WHERE (nom_commune = ? OR ? IS NULL) 
    AND (code_postal = ? OR ? IS NULL) 
    AND (adresse_nom_voie = ? OR ? IS NULL) 
    AND (code_type_local = ? OR ? IS NULL);
    """
    mycursor.execute(query, (nom_commune, nom_commune, code_postal, code_postal, adresse_nom_voie, adresse_nom_voie, code_type_local, code_type_local,))
    result = mycursor.fetchall()

    # Récupération des marqueurs (longitude et latitude)
    # On itére sur chaque dictionnaire pour récupérer les coordonnées géographiques
    longitudes = [float(d['longitude']) for d in result if d['longitude']]
    latitudes = [float(d['latitude']) for d in result if d['latitude']]

    # affichage des marqueurs
    fig = go.Figure(go.Scattermapbox(
        lat=latitudes,
        lon=longitudes,
        mode='markers',
        marker=go.scattermapbox.Marker(size=14),
        text=[f"{d['adresse_numero']} {d['adresse_nom_voie']}, {d['nom_commune']}, {d['valeur_fonciere']}" for d in result],
    ))

    fig.update_layout(
        mapbox={
            'style': "carto-positron",
            'center': {'lon': 1.429504, 'lat': 43.598938},
            'zoom': 12
        },
        width=1200,
        height=900
    )

    # Sauvegarde du fichier html dans un fichier temporaire.
    tmp_html_file = 'temp_map.html'
    fig.write_html(tmp_html_file)

    with open(tmp_html_file, 'r') as f:
        src_doc = f.read()

    return src_doc

if __name__ == '__main__':
    app.run_server(debug=True)