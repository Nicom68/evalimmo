-- Recherche par commune ou nom de rue de bien_immobilier, affichage de la surface habitable en m2, du type de bien
  
SELECT adresse_nom_voie, nom_commune, surface_reelle_bati, code_type_local
FROM bien_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
JOIN lot ON bien_immo.id_immo = lot.id_immo
WHERE adresse_nom_voie = "AV CRAMPEL";