SELECT *
FROM bien_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE surface_terrain = (SELECT MAX(surface_terrain) FROM bien_immo);
