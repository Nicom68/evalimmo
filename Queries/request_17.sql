--  17) Trouver le nombre de lots pour 6 rue d'Aubuisson 

SELECT id_mutation, numero_lot, adresse_numero, adresse_nom_voie
FROM lot
JOIN bien_immo ON lot.id_immo = bien_immo.id_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE adresse_nom_voie = "RUE D'AUBUISSON" AND adresse_numero="6";