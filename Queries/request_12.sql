SELECT *
FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE vente.valeur_fonciere = (
    SELECT MAX(valeur_fonciere)
    FROM vente
);
