-- 4) bien immobilier ayant fait l'objet de plusieurs ventes dans l'année
SELECT adresse.*
FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
WHERE bien_immo.id_immo IN (
    SELECT vente_immo.id_immo
    FROM vente_immo
    JOIN vente ON vente_immo.id_vente = vente.id_vente
    GROUP BY vente_immo.id_immo
    HAVING COUNT(vente.id_mutation) > 5
);