-- Recherche par commune de bien_immobilier, affichage de la valeur foncière, du type de bien, et du numéro de rue
SELECT adresse_nom_voie, nom_commune, valeur_fonciere, code_type_local
FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN vente ON vente_immo.id_vente = vente.id_vente
WHERE "nom_commune" = "Balma"