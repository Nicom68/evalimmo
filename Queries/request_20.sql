-- 20) Recherche du nombre de transactions par commune (classement par ordre décroissant)

SELECT nom_commune, COUNT(id_mutation) AS nombre_ventes
FROM VENTE
JOIN vente_immo ON vente.id_vente = vente_immo.id_vente
JOIN bien_immo ON bien_immo.id_immo = vente_immo.id_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
GROUP BY nom_commune
ORDER BY nombre_ventes DESC;