-- 3) comptage des mutations pour une adresse avec numéro (5 AV JEAN RIEUX)
SELECT COUNT (id_mutation)
FROM vente
JOIN vente_immo ON vente.id_vente = vente_immo.id_vente
JOIN bien_immo ON vente_immo.id_immo = bien_immo.id_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
WHERE adresse_nom_voie = 'AV JEAN RIEUX' AND adresse_numero = 5
ORDER BY adresse_numero;
