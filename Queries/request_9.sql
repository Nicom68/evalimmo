-- - 9) test comptage des mutations (bien_immo + adresse + valeur_immo + id_parcelle)

SELECT COUNT (id_mutation)
FROM vente
JOIN vente_immo ON vente.id_vente = vente_immo.id_vente
JOIN bien_immo ON vente_immo.id_immo = bien_immo.id_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN lot ON bien_immo.id_immo=lot.id_immo
WHERE adresse_nom_voie = 'AV JEAN RIEUX' AND id_parcelle="31555810AC0332"
ORDER BY adresse_numero;
