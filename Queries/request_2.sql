-- Recherche biens immobiliers à l'adresse "5, Avenue Jean Rieux"
SELECT adresse_numero, adresse_nom_voie, numero_lot
FROM bien_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN lot ON bien_immo.id_immo = lot.id_immo
WHERE adresse_numero = 5 AND adresse_nom_voie="AV JEAN RIEUX";