-- -18) Recherche de bien avec un parc à Toulouse
SELECT DISTINCT id_parcelle,adresse_nom_voie, nom_commune, surface_terrain, nature_culture_speciale
FROM bien_immo
JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
JOIN terrain ON bien_immo.id_immo = terrain.id_immo
JOIN culture_speciale ON terrain.code_nature_culture_speciale = culture_speciale.code_nature_culture_speciale
WHERE culture_speciale.code_nature_culture_speciale = 'PARC' AND nom_commune = 'Toulouse';