-- - 8) test comptage des biens vendus par adresse (bien_immo + adresse + valeur_immo + id_parcelle)
SELECT DISTINCT(id_mutation) FROM adresse
JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
JOIN vente ON bien_immo.id_immo = vente.id_vente
WHERE adresse_nom_voie = "RUE BAYARD";