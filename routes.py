from typing import Optional, List, Dict
from fastapi import FastAPI, Query
import sqlite3

# Création de l'instance FastAPI
app = FastAPI()



# 1). Route pour récupérer la valeur foncière des biens immobiliers  par commune ou rue et par type de logement
@app.get("/bien_immo/valeur_fonciere")
def get_valeur_fonciere(
    nom_commune: Optional[str] = Query(None),
    adresse_nom_voie: Optional[str] = Query(None),
    code_type_local: Optional[int] = Query(None)
):
    # connexion à la base de données
    connection = sqlite3.connect("evalimmo.db")
    connection.row_factory = sqlite3.Row  # retourne des dictionnaires
    cursor = connection.cursor()

    query = """
        SELECT adresse_nom_voie, nom_commune, valeur_fonciere, code_type_local
        FROM adresse
        JOIN bien_immo ON adresse.id_adresse = bien_immo.id_adresse
        JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
        JOIN vente ON vente_immo.id_vente = vente.id_vente
        WHERE 1=1
    """

    parameters = []
    if nom_commune:
        query += " AND adresse.nom_commune = ?"
        parameters.append(nom_commune)
    if adresse_nom_voie:
        query += " AND adresse.adresse_nom_voie = ?"
        parameters.append(adresse_nom_voie)
    if code_type_local:
        query += " AND bien_immo.code_type_local = ?"
        parameters.append(code_type_local)

    cursor.execute(query, parameters)
    rows = cursor.fetchall()
    cursor.close()
    connection.close()
    
    return [dict(row) for row in rows]

# # 2). Route pour récupérer la surface des logements par commune ou nom de rue et par type de logements
# @app.get("/bien_immo/surface_logement")
# def get_surface_logement(
#     nom_commune: Optional[str] = Query(None),
#     adresse_nom_voie: Optional[str] = Query(None),
#     code_type_local: Optional[int] = Query(None)
# ):
#     # connexion à la base de données
#     connection = sqlite3.connect("evalimmo.db")
#     connection.row_factory = sqlite3.Row  # retourne des dictionnaires
#     cursor = connection.cursor()

#     query = """
#         SELECT adresse_nom_voie, nom_commune, surface_reelle_bati, code_type_local
#     FROM bien_immo
#     JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
#     JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
#     JOIN vente ON vente_immo.id_vente = vente.id_vente
#     JOIN lot ON bien_immo.id_immo = lot.id_immo
#     """

#     parameters = []
#     if nom_commune:
#         query += " AND adresse.nom_commune = ?"
#         parameters.append(nom_commune)
#     if adresse_nom_voie:
#         query += " AND adresse.adresse_nom_voie = ?"
#         parameters.append(adresse_nom_voie)
#     if code_type_local:
#         query += " AND bien_immo.code_type_local = ?"
#         parameters.append(code_type_local)

#     cursor.execute(query, parameters)
#     rows = cursor.fetchall()
#     cursor.close()
#     connection.close()
    
#     return [dict(row) for row in rows]

# 3). Route pour récupérer la valeur moyenne d'un bien par commune ou nom de rue et par type de bien

# @app.get("/bien_immo/valeur_moyenne")
# def get_valeur_moyenne(
#     nom_commune: Optional[str] = Query(None),
#     adresse_nom_voie: Optional[str] = Query(None),
#     code_type_local: Optional[int] = Query(None)
# ):
#     connection = sqlite3.connect("evalimmo.db")
#     connection.row_factory = sqlite3.Row
#     cursor = connection.cursor()

#     query = """
#         SELECT 
#             AVG(valeur_fonciere) AS valeur_fonciere_moyenne,
#             AVG(surface_reelle_bati) AS surface_reelle_bati_moyenne
#         FROM bien_immo
#         JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
#         JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
#         JOIN vente ON vente_immo.id_vente = vente.id_vente
#         JOIN lot ON bien_immo.id_immo = lot.id_immo
#         WHERE 1=1
#     """

#     parameters = []
#     if nom_commune:
#         query += " AND adresse.nom_commune = ?"
#         parameters.append(nom_commune)
#     if adresse_nom_voie:
#         query += " AND adresse.adresse_nom_voie = ?"
#         parameters.append(adresse_nom_voie)
#     if code_type_local:
#         query += " AND bien_immo.code_type_local = ?"
#         parameters.append(code_type_local)

#     cursor.execute(query, parameters)
#     row = cursor.fetchone()
#     cursor.close()
#     connection.close()
    
#     if row:
#         return {
#             "valeur_fonciere_moyenne": row["valeur_fonciere_moyenne"],
#             "surface_reelle_bati_moyenne": row["surface_reelle_bati_moyenne"]
#         }
#     else:
#         return {"average_valeur_fonciere": None, "average_surface_reelle_bati": None}


# 4). Route pour récupérer la valeur moyenne au m2 d'un bien pour une commune ou nom de rue et par type de bien

# @app.get("/bien_immo/valeur_moyenne_m2")
# def get_valeur_moyenne_m2(
#     nom_commune: Optional[str] = Query(None),
#     adresse_nom_voie: Optional[str] = Query(None),
#     code_type_local: Optional[int] = Query(None)
# ):
#     connection = sqlite3.connect("evalimmo.db")
#     connection.row_factory = sqlite3.Row
#     cursor = connection.cursor()

#     query = """
#         SELECT 
#             AVG(valeur_fonciere) AS valeur_fonciere_moyenne,
#             AVG(surface_reelle_bati) AS surface_reelle_bati_moyenne
#         FROM bien_immo
#         JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
#         JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
#         JOIN vente ON vente_immo.id_vente = vente.id_vente
#         JOIN lot ON bien_immo.id_immo = lot.id_immo
#         WHERE 1=1
#     """

#     parameters = []
#     if nom_commune:
#         query += " AND adresse.nom_commune = ?"
#         parameters.append(nom_commune)
#     if adresse_nom_voie:
#         query += " AND adresse.adresse_nom_voie = ?"
#         parameters.append(adresse_nom_voie)
#     if code_type_local:
#         query += " AND bien_immo.code_type_local = ?"
#         parameters.append(code_type_local)

#     cursor.execute(query, parameters)
#     row = cursor.fetchone()
#     cursor.close()
#     connection.close()
    
#     if row and row["surface_reelle_bati_moyenne"] > 0:
#         valeur_moyenne_m2 = row["valeur_fonciere_moyenne"] / row["surface_reelle_bati_moyenne"]
#     else:
#         valeur_moyenne_m2 = None

#     return {
#         "valeur_fonciere_moyenne": row["valeur_fonciere_moyenne"],
#         "surface_reelle_bati_moyenne": row["surface_reelle_bati_moyenne"],
#         "valeur_moyenne_m2": valeur_moyenne_m2
#     }
# Mise en marche de l'application FastAPI
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8000)






       

        
        
     





