import dash
from dash import html, dcc
from dash.dependencies import Input, Output
import plotly.graph_objects as go
import sqlite3
from app_layout_moy import create_layout

# Initialisation de l'application DASH
app = dash.Dash(__name__)

# Récupération de la mise en page à partir du fichier "app_layout.py"
app.layout = create_layout()


@app.callback(
    [Output('map-iframe', 'srcDoc'),
     Output('average_valeur_m2', 'children')],
    [Input('nom_commune', 'value'),
     Input('code_postal', 'value'),
     Input('adresse_nom_voie', 'value'),
     Input('code_type_local', 'value')]
)

# connection à la base de données

def update_map(nom_commune, code_postal, adresse_nom_voie, code_type_local):
    connection = sqlite3.connect("evalimmo.db")
    connection.row_factory = sqlite3.Row  # return dictionaries
    mycursor = connection.cursor()

# Exécution de la requête

    query = """ 
    SELECT 
        AVG(vente.valeur_fonciere) AS valeur_fonciere_moyenne,
        AVG(bien_immo.surface_reelle_bati) AS surface_reelle_bati_moyenne,
        adresse.latitude,
        adresse.longitude,
        adresse.adresse_numero,
        adresse.adresse_nom_voie,
        adresse.nom_commune,
        vente.valeur_fonciere
    FROM bien_immo
    JOIN adresse ON bien_immo.id_adresse = adresse.id_adresse
    JOIN vente_immo ON bien_immo.id_immo = vente_immo.id_immo
    JOIN vente ON vente_immo.id_vente = vente.id_vente
    JOIN lot ON bien_immo.id_immo = lot.id_immo
    WHERE (adresse.nom_commune = ? OR ? IS NULL)
      AND (adresse.code_postal = ? OR ? IS NULL)
      AND (adresse.adresse_nom_voie = ? OR ? IS NULL)
      AND (bien_immo.code_type_local = ? OR ? IS NULL)
    """
    
    params = (nom_commune, nom_commune, code_postal, code_postal, adresse_nom_voie, adresse_nom_voie, code_type_local, code_type_local)
    mycursor.execute(query, params)
    result = mycursor.fetchall()
    
    # Récupération de tous les coordonnées géographiques des biens immobiliers
    longitudes = [float(d['longitude']) for d in result if d['longitude']]
    latitudes = [float(d['latitude']) for d in result if d['latitude']]

    # Création des marqueurs
    fig = go.Figure(go.Scattermapbox(
        lat=latitudes,
        lon=longitudes,
        mode='markers',
        marker=go.scattermapbox.Marker(
            size=14
        ),
        text=[f"{d['adresse_numero']} {d['adresse_nom_voie']}, {d['nom_commune']}, {d['valeur_fonciere']}" for d in result],
    ))

    fig.update_layout(
        mapbox={
            'style': "carto-positron",
            'center': {'lon': 1.429504, 'lat': 43.598938},
            'zoom': 12
        },
        width=1200,
        height=900
    )
    
    # Sauvegarde du fichier html
    tmp_html_file = 'temp_map.html'
    fig.write_html(tmp_html_file)

    with open(tmp_html_file, 'r') as f:
        src_doc = f.read()
    
    # Calcul de la valeur moyenne au m2
    if result:
        valeur_fonciere_moyenne = result[0]['valeur_fonciere_moyenne']
        surface_reelle_moyenne = result[0]['surface_reelle_bati_moyenne']
        if surface_reelle_moyenne:
            valeur_moyenne_m2 = valeur_fonciere_moyenne / surface_reelle_moyenne
        else:
            valeur_moyenne_m2 = 'N/A'
    else:
        valeur_moyenne_m2 = 'N/A'
    
    valeur_moyenne_m2_text = f"Valeur moyenne par m²: {valeur_moyenne_m2}"
    
    return src_doc, valeur_moyenne_m2_text

if __name__ == '__main__':
    app.run_server(debug=True)