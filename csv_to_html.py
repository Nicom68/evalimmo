import pandas as pd

df = pd.read_csv('31.csv')
print(df)

# Conversion du tabea
html_table = df.to_html(index=False)

# Generate HTML page with the table
html_page = f"""
<!DOCTYPE html>
<html>
<head>
    <title>Ventes 31 Immo</title>
    <style>
        table {{
            border-collapse: collapse;
            width: 100%;
        }}
        th, td {{
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }}
        th {{
            background-color: #f2f2f2;
        }}
    </style>
</head>
<body>

<h2>Immoval</h2>
{html_table}

</body>
</html>
"""

# Write HTML page to a file
with open('ventes31.html', 'w') as f:
    f.write(html_page)

print("HTML page generated successfully.")